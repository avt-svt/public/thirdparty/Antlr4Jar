cmake_minimum_required(VERSION 3.11)
project(Antlr4Jar)

set(ANTLR4_JAR_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/antlr-4.7.2-complete.jar)
set(ANTLR4_JAR_LOCATION ${ANTLR4_JAR_LOCATION} PARENT_SCOPE)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "Antlr4JarConfigVersion.cmake"
    VERSION 4.7.2
    COMPATIBILITY ExactVersion)

install(FILES "Antlr4JarConfig.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/Antlr4JarConfigVersion.cmake"
    DESTINATION lib/cmake/Antlr4Jar COMPONENT Antlr4Jar)

install(FILES antlr-4.7.2-complete.jar DESTINATION lib COMPONENT Antlr4Jar)

### CPACK stuff ###
set(CPACK_PACKAGE_NAME "Antlr4Jar")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Java archive of ANTLR4 (complete)")
SET(CPACK_PACKAGE_VENDOR "raha01")
SET(CPACK_PACKAGE_VERSION_MAJOR "4")
SET(CPACK_PACKAGE_VERSION_MINOR "7")
SET(CPACK_PACKAGE_VERSION_PATCH "2")
#SET(CPACK_IFW_VERBOSE ON)
include(CPack)
include(CPackIFW)
cpack_add_component(Antlr4Jar DISPLAY_NAME "Antlr4Jar" DESCRIPITON "Java archive of ANTLR4 (complete)")
cpack_ifw_configure_component(Antlr4Jar
    VERSION "4.7.2" # Version of component
    DEFAULT TRUE)
